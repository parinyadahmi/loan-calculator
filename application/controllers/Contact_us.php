<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
	}
	
	public function index(){	
		// load view
		$this->load->view('config/header');
		$this->load->view('contact-us');
		$this->load->view('config/footer');
	}	
}
