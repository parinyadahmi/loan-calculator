
<div id="outer-wrap">
	<div id="inner-wrap">

		<header id="top" role="banner">
			<div class="block">
				<h1 class="block-title">Loan Calculation</h1>
				<a class="nav-btn" id="nav-open-btn" href="#nav">Housing Loan
					Calculate</a>
			</div>
		</header>

		<nav id="nav" role="navigation">
			<div class="block">
				<h2 class="block-title">Chapters</h2>
				<ul>
					<li><?php echo anchor('home/calculate','Home'); ?></li>
					<li><?php echo anchor('type_of_loan','Manage type of loan'); ?></li>
					<li><?php echo anchor('contact_us','Contact us'); ?></li>
				</ul>
				<a class="close-btn" id="nav-close-btn" href="#top">Return to
					Content</a>
			</div>
		</nav>

		<div class="container">
			<div class="row">
				<div class="col-md-2 pull-right text-right"
					style="padding-top: 10px; cursor: pointer;">

					<a class="" style="text-decoration: none; color: black;"
						data-toggle="modal" data-target="#myModal1">Login</a>

					<div class="modal fade" id="myModal1" tabindex="-1" role="dialog"
						aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal"
										aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<h4 class="text-left" id="myModalLabel">Sign In</h4>
								</div>
								<div class="modal-body">

										<form class="omb_loginForm" action="<?php echo base_url(); ?>index.php/Type_of_loan/add" autocomplete="off"
													method="post">
										<div class="row omb_row-sm-offset-3">
											<div class="col-xs-12 col-sm-12">
											
											
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-user"></i></span>
														<input type="text" class="form-control" name="username"
															placeholder="username">
													</div>
													<span class="help-block"></span>

													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-lock"></i></span>
														<input type="password" class="form-control"
															name="password" placeholder="Password">
													</div>
													<br>

													<button class="btn btn-sm btn-primary btn-block"
														type="submit">Login</button>
											</div>
										</div>
									</form>
								</div>
							</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>