<!DOCTYPE html>
<html class="no-js">
<head>
<meta charset="utf-8">
<title>Loan Calculator</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- css -->
<link
	href="<?php echo base_url(); ?>bower_components/bootstrap/dist/css/step4.css"
	rel="stylesheet">
<link
	href="<?php echo base_url(); ?>bower_components/bootstrap/dist/css/bootstrap.css"
	rel="stylesheet">
<link
	href="<?php echo base_url(); ?>bower_components/bootstrap/dist/css/bootstrap-theme.css"
	rel="stylesheet">

<link
	href="<?php echo base_url(); ?>bower_components/fontawesome/css/font-awesome.min.css"
	rel="stylesheet">

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<script src="bower_components/bootstrap/js/modernizr.js"></script>

</head>

<body>
	<div id="main" role="main">
		<article class="block prose container">

			<div class="row">
				<div class="col-md-3 col-sm-3">	
				<?php include 'config/menu-sidebar.php';?>
				</div>

				<div class=" col-md-9 col-sm-9">
					<div class="panel panel-default">
						<div class="panel-body">

							<h2 class="text-center">
								<span class=""></span><strong> Edit type of loan </strong>
							</h2><br>

							<?php echo form_open('type_of_loan/edit/'.$this->uri->segment(3));?>
							<div class="row">
								<form class="form-horizontal">

									<div class="form-group">
										<span class="col-md-2 control-label">Type of loan</span>
										<div class=" col-md-7">
											<input type="text" class="form-control input-sm"
												name="type_of_loan"
												value="<?php echo $rs['type_of_loan'];?>"
												required="required">
										</div>
										<div class="clearfix"></div>
									</div>

									<div class="form-group">
										<span class="col-md-2 control-label">Interest (%)</span>
										<div class="col-md-7">
											<input type="text" class="form-control input-sm"
												name="interest" value="<?php echo $rs['interest'];?>"
												required="required">

										</div>
										<div class="clearfix"></div>
									</div>

									<div class="form-group">
										<div class="col-md-offset-2 col-md-8">

											<input class="btn btn-default" type="submit" name="btsave"
												value="Confirm" />
										 <?php echo anchor(base_url().'index.php/type_of_loan','<div class="btn btn-default"><span class="txt_white">Cancle</span></div>'); ?>
									</div>
									</div>
								</form>
							</div>
							<?php echo form_close(); ?>
							
						</div>
					</div>
				</div>
			</div>
		</article>
	</div>
	<div style="padding-top: 100px;"></div>

</body>
</html>